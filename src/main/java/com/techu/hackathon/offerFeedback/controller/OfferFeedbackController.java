package com.techu.hackathon.offerFeedback.controller;

import com.techu.hackathon.offerFeedback.model.OfferFeedbackModel;
import com.techu.hackathon.offerFeedback.service.OfferFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/offerFeedback")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class OfferFeedbackController {

    @Autowired
    OfferFeedbackService offerFeedbackService;


    @PostMapping()
    public ResponseEntity<Object> nuevaCuenta(@RequestBody OfferFeedbackModel ofertaFeedback)
    {
        offerFeedbackService.nuevaOfertaFeedback(ofertaFeedback);
        return ResponseEntity.ok("Todo OK");
    }
}
