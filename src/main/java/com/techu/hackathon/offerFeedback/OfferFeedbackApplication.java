package com.techu.hackathon.offerFeedback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfferFeedbackApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfferFeedbackApplication.class, args);
	}

}
