package com.techu.hackathon.offerFeedback.service;

import com.techu.hackathon.offerFeedback.model.OfferFeedbackModel;
import com.techu.hackathon.offerFeedback.repository.OfferFeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("ofertaFeedbackService")
@Transactional
public class OfferFeedbackServiceImpl implements OfferFeedbackService{
    OfferFeedbackRepository offerFeedbacRepository;

    @Autowired
    public OfferFeedbackServiceImpl(OfferFeedbackRepository offerFeedbacRepository){
        this.offerFeedbacRepository = offerFeedbacRepository;}

    @Override
    public void nuevaOfertaFeedback(OfferFeedbackModel ofertaFeedback) {
        offerFeedbacRepository.nuevaOfertaFeedback(ofertaFeedback);
    }
}
