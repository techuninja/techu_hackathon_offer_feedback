package com.techu.hackathon.offerFeedback.service;

import com.techu.hackathon.offerFeedback.model.OfferFeedbackModel;

public interface OfferFeedbackService {

    void nuevaOfertaFeedback(OfferFeedbackModel ofertaFeedback);
}
