package com.techu.hackathon.offerFeedback.model;


import org.jetbrains.annotations.NotNull;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "offerFeedback")
public class OfferFeedbackModel {

    @NotNull
    private String offerCode;
    @NotNull
    private String codigoCliente;
    private String abierto;
    private String aceptado;
    private String fecha;

    public OfferFeedbackModel(String offerCode, String codigoCliente, String abierto, String aceptado, String fecha){
        this.setOfferCode(offerCode);
        this.setCodigoCliente(codigoCliente);
        this.setAbierto(abierto);
        this.setAceptado(aceptado);
        this.setFecha(fecha);
    }

    public String getOfferCode() {
        return offerCode;
    }

    public void setOfferCode(String offerCode) {
        this.offerCode = offerCode;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getAbierto() {
        return abierto;
    }

    public void setAbierto(String abierto) {
        this.abierto = abierto;
    }

    public String getAceptado() {
        return aceptado;
    }

    public void setAceptado(String aceptado) {
        this.aceptado = aceptado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
