package com.techu.hackathon.offerFeedback.repository;

import com.techu.hackathon.offerFeedback.model.OfferFeedbackModel;

public interface OfferFeedbackRepository {

    void nuevaOfertaFeedback(OfferFeedbackModel ofertaFeedback);
}
