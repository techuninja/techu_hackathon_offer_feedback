package com.techu.hackathon.offerFeedback.repository;

import com.techu.hackathon.offerFeedback.model.OfferFeedbackModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

@Repository
public class OfferFeedbackRepositoryImpl implements OfferFeedbackRepository{
    private final MongoOperations mongoOperations;

    @Autowired
    public OfferFeedbackRepositoryImpl(MongoOperations mongoOperations){this.mongoOperations=mongoOperations;}

    @Override
    public void nuevaOfertaFeedback(OfferFeedbackModel ofertaFeedback) {
        this.mongoOperations.save(ofertaFeedback);
    }
}
