FROM openjdk:11-jre-slim-buster
EXPOSE 8086
ARG JAR_FILE=target/offerFeedback-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} offer_feedback.jar
ENTRYPOINT ["java","-jar","/offer_feedback.jar"]